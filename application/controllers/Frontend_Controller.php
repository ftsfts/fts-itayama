<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once (dirname(__FILE__) . '/../third_party/Smarty-3.1.18/libs/Smarty.class.php');
/**
 *
 * @author Nobutaka Tanaka
 */
class Frontend_Controller extends CI_Controller
{
    const DIR_SMARTY_LAYOUT = 'layout';
    const DIR_SMARTY_PAGE = 'page';
    const DIR_SMARTY_PARTIAL = 'partial';

    // smartyインスタンス
    protected $_smarty;
    // 基本レイアウトを設定する変数
    protected $_layout_name = 'default.tpl';
    // レイアウト内部から参照されるページ名称
    protected $_page_name;


    public function __construct()
    {
        parent::__construct();
        // smarty初期化
        $this->_smarty = new Smarty();
        $this->_smarty->template_dir = APPPATH . '/views/templates';
        $this->_smarty->compile_dir = APPPATH . '/views/templates_c';
        $this->_smarty->config_dir = APPPATH . '/views/configs';
        $this->_smarty->cache_dir = APPPATH . '/views/cache';

        $this->load->helper('string');
    }

    /**
     * Smartyを挟み込み
     */
    public function _output($output)
    {
        if (strlen($output) > 0) {
            echo $output;

        } else {
            $this->_smarty->assign('template_dir', current($this->_smarty->template_dir));
            $this->_smarty->assign('base_assets_url', $this->config->config['base_assets_url']);
            $this->_smarty->assign('page_assets_url', concat_unix_pathes(array($this->config->config['base_assets_url'], $this->_page_name)));
            $this->_smarty->assign('action_name', $this->_action_name);
            $this->_smarty->assign('page_name', concat_unix_pathes(array(self::DIR_SMARTY_PAGE, $this->_page_name)));
            $this->_smarty->display(concat_unix_pathes(array(self::DIR_SMARTY_LAYOUT, $this->_layout_name)));
        }
    }

    /**
     * 認証エラーページを表示する.
     * 呼び出す際は参照元でdie(0)などを明示してあげると親切かも
     *
     * @param string $page_name
     */
    protected function _show_401($page_name = '401.tpl')
    {
        $this->output->set_status_header('401');
        $this->_page_name = $page_name;
        $this->CI = &get_instance();
        $this->CI->output->_display();
        die(0);
    }

    /**
     * リダイレクトする
     *
     * @param string $url リダイレクト先URL
     */
    protected function _redirect($url)
    {
        header('Location: ' . $url);
        die(0);
    }

    /**
     *
     */
    protected function _logger($class, $line, $message)
    {
        $output = sprintf('file: %s, line: %s, message: %s', $class, $line, $message);
        error_log($output);
    }

}

