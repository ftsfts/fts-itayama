<?php
require dirname(__FILE__) . '/form_field.php';
//
$config['form_validation']['error_prefix'] = '';
$config['form_validation']['error_suffix'] = '';

// nanaco_id
$field = 'nanaco_id';
$label = 'nanaco ID';
$rules = 'required';

$validation = [
  'field' => $field,
  'label' => $label,
  'rules' => $rules,
  'errors' => ['required' => 'nanaxo_idを入力してください']
];

$config['form_validation'][] = $validation;

// sex
$values = $config['form_field']['sex']['values'];
$in_list = implode(',', $values);;
$field = 'sex';
$label = 'sex';
$rules = "in_list[$in_list]";
//$rules = "required";

$validation = [
  'field' => $field,
  'label' => $label,
  'rules' => $rules,
  'errors' => [
               'required' => '※性別を選択してください',
               'in_list' => '※性別を選択してください'
              ]
];
$config['form_validation'][] = $validation;

// birth_year
$values = $config['form_field']['birth_year']['values'];
$in_list = implode(',', $values);;
$field = 'birth_year';
$label = 'birth_year';
$rules = "required|in_list[$in_list]";

$validation = [
  'field' => $field,
  'label' => $label,
  'rules' => $rules,
  'errors' => [
               'required' => '※年を選択してください',
               'in_list' => '※年を選択してください'
              ]
];

$config['form_validation'][] = $validation;

// birth_month
$values = $config['form_field']['birth_month']['values'];
$in_list = implode(',', $values);;
$field = 'birth_month';
$label = 'birth_month';
$rules = "required|in_list[$in_list]";

$validation = [
  'field' => $field,
  'label' => $label,
  'rules' => $rules,
  'errors' => [
               'required' => '※月を選択してください',
               'in_list' => '※月を選択してください'
              ]
];

$config['form_validation'][] = $validation;
