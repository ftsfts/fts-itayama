<?php
// DB用設定ファイル

// ybdp【master】
$db['ybdp_master']['hostname'] = "";
$db['ybdp_master']['username'] = "";
$db['ybdp_master']['password'] = "";
$db['ybdp_master']['database'] = "";
$db['ybdp_master']['dbdriver'] = "mysqli";
$db['ybdp_master']['dbprefix'] = "";
$db['ybdp_master']['pconnect'] = FALSE;
$db['ybdp_master']['db_debug'] = FALSE;
$db['ybdp_master']['cache_on'] = FALSE;
$db['ybdp_master']['cachedir'] = "";
$db['ybdp_master']['char_set'] = "sjis";
$db['ybdp_master']['dbcollat'] = "sjis";
$db['ybdp_master']['swap_pre'] = "";
$db['ybdp_master']['autoinit'] = FALSE;
$db['ybdp_master']['stricton'] = FALSE;


// ybdp【slave】
$db['ybdp_slave'] = $db['ybdp_master'];
