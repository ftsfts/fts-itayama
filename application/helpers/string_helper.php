<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
// 文字列操作に関するヘルパークラス

/**
 * unixパスを連結して返却する
 */
function concat_unix_path($path1, $path2, $addPostSlash=false)
{
    $path1_flg = substr($path1, -1, 1) === '/' ? true : false;
    $path2_flg = substr($path2, 0, 1) === '/' ? true : false;

    if ($path1_flg === true && $path2_flg === true) {
        $concatPath = $path1 . substr($path2, 1);

    } else if ($path1_flg === true || $path2_flg === true) {
        $concatPath = $path1 . $path2;

    } else {
        $concatPath = $path1 . '/' . $path2;

    }

    if (($addPostSlash === true) && (substr($concatPath, -1, 1) !== '/')) {
        $concatPath .= '/';
    }

    return $concatPath;
}

/**
 * 複数のunixパスを連結して返却する
 */
function concat_unix_pathes($pathes, $addPostSlash=false)
{
    foreach ($pathes as $path) {
        if (!isset($concatPath)) {
            $concatPath = $path;
            continue;
        }
        $concatPath = concat_unix_path($concatPath, $path);
    }

    if (($addPostSlash === true) && (substr($concatPath, -1, 1) !== '/')) {
        $concatPath .= '/';
    }

    return $concatPath;
}