<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * IDKANRI4LPテーブルに対応するDAO
 *
 * @author Nobutaka Tanaka
 */
class Mysql_Base_Dao extends CI_Model
{
    const YBDP_MASTER = 'ybdp_master';
    const YBDP_SLAVE = 'ybdp_slave';

    protected $_table_name;

    public function __construct()
    {

    }

    /**
     * データをロードする
     *
     * @param string $scheme スキームセット
     * @param string $absolute_path sqlファイルパス
     */
    public function load_sql_file($scheme, $absolute_path)
    {
        $this->load->database($scheme);

        if (file_exists($absolute_path) === false) {
            return array(
                        'result' => false,
                        'output' => 'file doesnt exists.'
                    );
        }

        $cmd = "mysql --user=".$this->db->username." --password=".$this->db->password." --host=".$this->db->hostname." < ".$absolute_path;
        $return;
        $output = array();

        exec($cmd, $output, $return);
        if ($return != 0) {
            return array(
                        'result' => false,
                        'return' => $return,
                        'output' => var_export($output, true)
                    );
        }

        return true;
    }

    /**
     * テーブルをトランケート
     *
     * @param string $scheme スキームセット
     * @param string|array $table_name テーブル名称
     */
    public function truncate_table($scheme, $table_name)
    {
        $this->load->database($scheme);

        if (is_array($table_name)) {
            foreach ($table_name as $name) {
                $this->db->truncate($name);
            }
        } else {
            $this->db->truncate($table_name);
        }

        $this->db->close();
    }

    /**
     * 全件取得する
     * 
     * @param string $scheme スキームセット
     */
    public function select_all($scheme)
    {
        $this->load->database($scheme);
        $query = $this->db->get($this->_table_name);
        $this->db->close();

        return $query->result_array();
    }

    /**
     * トランザクションラッパ
     */
    public function begin()
    {
        $this->db->trans_start();
    }

    /**
     * コミット
     */
    public function commit()
    {
        $this->db->trans_complete();
    }

    /**
     * ロールバック
     */
    public function rollback()
    {
        $this->db->rollback();
    }
}
