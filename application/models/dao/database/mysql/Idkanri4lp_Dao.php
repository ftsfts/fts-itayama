<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author Nobutaka Tanaka
 */
require_once (dirname(__FILE__) . '/mysql_base_dao.php');

class Idkanri4lp_Dao extends Mysql_Base_Dao
{
    const COLUMN_TONARIWA_ID = 'TONARIWA_ID';
    const COLUMN_NANACO_ID = 'NANACO_ID';
    const COLUMN_PEOPLE_ID = 'PEOPLE_ID';
    const COLUMN_CP_ID = 'CP_ID';
    const COLUMN_SEX = 'SEX';
    const COLUMN_BIRTH_YEAR = 'BIRTH_YEAR';
    const COLUMN_BIRTH_MONTH = 'BIRTH_MONTH';
    const COLUMN_REGISTER_DATE = 'REGISTER_DATE';

    public function __construct()
    {
        parent::__construct();
        $this->_table_name = 'IDKANRI4LP';
    }

    /**
     * 初期化処理
     */
    public function init()
    {
    }

    /**
     * 挿入処理
     */
    public function insert(array $data): ?int
    {
        $this->load->database(parent::YBDP_MASTER);
        $query = $this->db->insert($this->_table_name, $data);
        $last_id = $this->db->insert_id();

        $this->db->close();

        // var_dump($this->db->last_query());

        return $last_id;
    }

    /**
     * @return 結果レコード
     */
    public function find_by_tonariwa_id(string $tonariwa_id) : ?array
    {
        $this->load->database(parent::YBDP_SLAVE);
        $select_columns = array(self::COLUMN_TONARIWA_ID, self::COLUMN_NANACO_ID, self::COLUMN_PEOPLE_ID, self::COLUMN_CP_ID, self::COLUMN_SEX, self::COLUMN_BIRTH_YEAR, self::COLUMN_BIRTH_MONTH, self::COLUMN_REGISTER_DATE);

        $this->db->select(implode(',', $select_columns));
        $this->db->where(self::COLUMN_TONARIWA_ID, $tonariwa_id);

        $query = $this->db->get($this->_table_name);
        $this->db->close();

        // var_dump($this->db->last_query());

        return $query->row_array();
    }

    /**
     * @return 結果レコード
     */
    public function find_by_nanaco_id(string $nanaco_id) : ?array
    {
        $this->load->database(parent::YBDP_SLAVE);
        $select_columns = array(self::COLUMN_TONARIWA_ID, self::COLUMN_NANACO_ID, self::COLUMN_PEOPLE_ID, self::COLUMN_CP_ID, self::COLUMN_SEX, self::COLUMN_BIRTH_YEAR, self::COLUMN_BIRTH_MONTH, self::COLUMN_REGISTER_DATE);

        $this->db->select(implode(',', $select_columns));
        $this->db->where(self::COLUMN_NANACO_ID, $nanaco_id);

        $query = $this->db->get($this->_table_name);
        $this->db->close();

        // var_dump($this->db->last_query());

        return $query->row_array();
    }

}
