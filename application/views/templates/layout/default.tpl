<!doctype html>
<html lang="ja">
<head>
<!--{include file="$page_name/partial/meta.tpl"}-->
</head>
<!--{if file_exists("`$template_dir``$page_name`/partial/`$action_name`_body.tpl")}-->
<!--{include file="`$template_dir``$page_name`/partial/`$action_name`_body.tpl"}-->
<!--{else}-->
<body>
<!--{/if}-->

<!--{include file="$page_name/$action_name.tpl"}-->

<!--{include file="$page_name/partial/footer.tpl"}-->

<!--{include file="$page_name/partial/script.tpl"}-->

</body>
</html>
