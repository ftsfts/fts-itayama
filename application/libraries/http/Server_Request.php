<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * HTTPサーバリクエスト関連処理.
 *
 * @author Nobutaka Tanaka
 */
class Server_Request
{
    protected $_script_name;
    protected $_method;
    protected $_user_agent;
    protected $_get_params;
    protected $_post_params;
    protected $_http_referer;


    function __construct()
    {
        $this->_script_name = $_SERVER['SCRIPT_NAME'];
        $this->_method = $_SERVER['REQUEST_METHOD'];
        $this->_user_agent = $_SERVER['HTTP_USER_AGENT'];
        $this->_get_params = $_GET;
        $this->_post_params = $_POST;
        $this->_params = array_merge($_GET, $_POST);
        $this->_http_referer = $_SERVER['HTTP_REFERER'] ?? '';
    }

    public function get_get_params()
    {
        return $this->_get_params;
    }

    public function get_post_params()
    {
        return $this->_post_params;
    }

    public function get_params()
    {
        return $this->_params;
    }

    public function get_script_name()
    {
        return $this->_script_name;
    }

    public function get_user_agent()
    {
        return $this->_user_agent;
    }

    public function is_pc()
    {
        if (preg_match('#iphone|ipad|ipod|android#i', $this->_user_agent))
        {
            return false;
        }
        return true;
    }

    public function is_ios_access_from_user_agent()
    {
        if (preg_match('#iphone|ipad|ipod#i', $this->_user_agent))
        {
            return TRUE;
        }
        return FALSE;
    }

    public function is_android_access_from_user_agent()
    {
        if (preg_match('#android#i', $this->_user_agent))
        {
            return TRUE;
        }
        return FALSE;
    }

    public function get_method()
    {
        return $this->_method;
    }

    public function get_http_referer()
    {
        return $this->_http_referer;
    }

}
