<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * tonariwa id に関するクラス
 *
 * @author Nobutaka Tanaka
 */
class Tonariwa_Id
{
    const TONARIWA_ID_CHARACTERS = 64;
    const TONARIWA_ID_PATTERN = '/[0-9a-z]{64}/';

    function __construct()
    {
    }

    /**
     * tonariwa idかどうか
     * チェック
     * 64文字
     * [0-9a-z]の文字列
     */
    public function is_tonariwa_id(string $tonariwa_id) :bool
    {
        if (mb_strlen($tonariwa_id) !== self::TONARIWA_ID_CHARACTERS) {
            return false;
        }
        preg_match(self::TONARIWA_ID_PATTERN, $tonariwa_id, $matches);
        if (empty($matches)) {
            return false;
        }
        return true;
    }

}
