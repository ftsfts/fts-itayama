<?php
function smarty_modifier_boolean_to_string($bool)
{
  if (is_bool($bool) === true && $bool === true) {
    $string = 'true';
  } else {
    $string = 'false';
  }
  return $string;
}
